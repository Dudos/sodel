<?php
    include_once("core/core.php");
    
    $currentPage = $_GET['page'] ?? 'index';
    
    if (!file_exists("templates/" . $currentPage . ".php")) {
        $currentPage = '404';
    }
    
    # Подключаем шапку сайта
    require_once("templates/layout/header.php");
    
    # Подключаем страницу сайта
    require_once("templates/" . $currentPage . ".php");
    
    # Подключаем футер сайта
    require_once("templates/layout/footer.php");
    